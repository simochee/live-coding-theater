const shortid = require('shortid');

module.exports = {
  build: {
    hardSource: true
  },
  css: [
    'reset.css',
    'highlight.js/styles/tomorrow.css'
  ],
  head: {
    title: '🎞 LIVE CODING THEATER',
    link: [
      { rel: 'stylesheet', href: '//fonts.googleapis.com/css?family=Source+Code+Pro|Charm' }
    ]
  },
  env: {
    token: shortid.generate()
  },
  plugins: [
    '@/plugins/highlightjs'
  ],
  modules: [
    'cookie-universal-nuxt',
    '@/server/socket/'
  ],
  srcDir: 'src'
};
