import handler from './handler';

export default function (this: any) {
  this.nuxt.hook('listen', (server, { host, port }) => {
    const io = require('socket.io')(server);

    io.on('connection', handler);
  });
};
