import consola from 'consola';

let questions: any[] = [];

export default function (socket) {
  socket.on('admin:connect', () => {
    console.log('admin connected');
  });

  socket.on('admin:add', (payload) => {
    questions.push(payload);
  });

  socket.on('admin:remove', (id) => {
    questions = questions.filter((q) => q.id !== id);
  });

  socket.on('admin:edit', (payload) => {
    questions = questions.map((q) => q.id === payload.id ? payload : q);
  });

  socket.on('admin:activete', (id) => {
    const q = questions.find((q) => q.id === id);

    socket.broadcast.emit('theater:update.question', q);
  });

  socket.on('editor:connect', (payload) => {
    socket.broadcast.emit('session:join', payload);
  });

  socket.on('editor:sourceCode', (payload) => {
    socket.broadcast.emit('theater:sourceCode', payload);
  });

  socket.on('editor:leave', (payload) => {
    socket.broadcast.emit('session:leave', payload);
  });

  socket.on('theater:connect', () => {
    socket.broadcast.emit('session:check');
    socket.broadcast.emit('theater:sync.question', questions);
  });

  socket.on('theater:sync.result', () => {
    socket.broadcast.emit('request:result');
  });

  socket.on('response:result', (payload) => {
    socket.broadcast.emit('theater:update.result', payload);
  });
};
