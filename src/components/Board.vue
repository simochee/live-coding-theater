<template lang="pug">
label.board-block
  .q Q
  textarea.field#board(
    rows="1"
    :value="message"
    @input="message = $event.target.value"
  )
</template>

<script lang="ts">
import { Component, Watch, Vue } from 'nuxt-property-decorator';
import autosize from 'autosize';

@Component
export default class Board extends Vue {
  message = '';

  beforeMount(): void {
    this.message = this.$cookies.get('message');
  }

  mounted(): void {
    autosize(document.getElementById('board'));
  }

  @Watch('message')
  onChangeMessage(): void {
    this.$cookies.set('message', this.message);
  }
}
</script>

<style lang="sass" scoped>
.board-block
  &
    position: relative
    display: block
    padding: 18px 48px 18px 62px
    border: 5px solid #eee

  & > .q
    position: absolute
    top: 50%
    left: 24px
    font-family: 'Charm', cursive
    font-size: 28px
    color: #252521
    transform: translateY(-50%)

  & > .field
    display: block
    width: 100%
    min-height: 1em
    padding: 0
    margin: 0
    font-family: 'Source Code Pro', monospace
    font-size: 18px
    line-height: 1.8em
    border: 0
    outline: 0
    resize: none
</style>
